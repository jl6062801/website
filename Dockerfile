FROM ubuntu:22.04
MAINTAINER JL MPANDE
RUN apt-get update
RUN apt-get install -y nginx
ADD static-website-example/ /var/www/html/
COPY nginx.conf /etc/nginx/conf.d/default.conf
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx, -g, 'daemon off;'
